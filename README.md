# SDK Update

## Update
Run the follow commands in the SDK root directory:  
```
# pull the bundle repo once
git clone https://gitlab.com/TeeFirefly/xxx-bundle.git .bundle
# update
.bundle/update
```
You can merger your source code after update(FETCH_HEAD branch).  
